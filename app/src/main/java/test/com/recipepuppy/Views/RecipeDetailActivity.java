package test.com.recipepuppy.Views;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import test.com.recipepuppy.Models.Recipe;
import test.com.recipepuppy.R;

/**
 * Created by Angel on 24/3/18.
 */

public class RecipeDetailActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Recipe recipe;
    private byte[] imgRecipe;

    private ImageView ivRecipe;
    private TextView tvName;
    private TextView tvIngredients;
    private TextView tvHref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            recipe = bundle.getParcelable(RecipeListActivity.RECIPE_ARG);
            imgRecipe = bundle.getByteArray(RecipeListActivity.IMAGE_RECIPE_ARG);
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivRecipe = findViewById(R.id.iv_recipe);
        tvName = findViewById(R.id.tv_name_recipe);
        tvIngredients = findViewById(R.id.tv_ingredients_recipe);
        tvHref = findViewById(R.id.tv_href_recipe);


        Bitmap bitmap = BitmapFactory.decodeByteArray(imgRecipe, 0, imgRecipe.length);

        ivRecipe.setImageBitmap(bitmap);
        tvName.setText(recipe.getTitle());
        tvIngredients.setText(recipe.getIngredients());
        tvHref.setText(recipe.getHref());
    }
}
