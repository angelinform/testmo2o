package test.com.recipepuppy.Views;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.com.recipepuppy.Models.Recipe;
import test.com.recipepuppy.Models.RecipeResponse;
import test.com.recipepuppy.Network.ApiUtils;
import test.com.recipepuppy.Network.RestClient;
import test.com.recipepuppy.R;

public class RecipeListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    public final static String RECIPE_ARG = "recipeArg";
    public final static String IMAGE_RECIPE_ARG = "imageRecipeArg";

    private RestClient restClient;
    private ArrayList<Recipe> recipes;
    private RecipesAdapter recipesAdapter;
    private RecyclerView recyclerViewRecipes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        restClient = ApiUtils.getRestClient();
        recyclerViewRecipes = findViewById(R.id.rv_recipes);

        callList();
    }

    private void callList() {
        restClient.getRecipes().enqueue(new Callback<RecipeResponse>() {
            @Override
            public void onResponse(Call<RecipeResponse> call, Response<RecipeResponse> response) {
                Log.i("RESPONSE", new Gson().toJson(response.body()));
                Log.i("RESPONSE", String.valueOf(response.code()));

                recipes = response.body().getResults();
                recipesAdapter = new RecipesAdapter(recipes);
                recipesAdapter.setOnItemClickListener(new RecipesAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Recipe recipe, byte[] imgRecipe) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(RECIPE_ARG, recipe);
                        bundle.putByteArray(IMAGE_RECIPE_ARG, imgRecipe);

                        Intent intent = new Intent(RecipeListActivity.this, RecipeDetailActivity.class);
                        intent.putExtras(bundle);
                        getApplication().startActivity(intent);
                    }
                });
                recyclerViewRecipes.setAdapter(recipesAdapter);
                recyclerViewRecipes.setLayoutManager(new LinearLayoutManager(RecipeListActivity.this, LinearLayoutManager.VERTICAL, false));
            }

            @Override
            public void onFailure(Call<RecipeResponse> call, Throwable t) {
                Log.i("FAILURE_RESPONSE", "Unable to get recipes");
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        recipesAdapter.filter(newText);
        return true;
    }

    public static class RecipesAdapter extends RecyclerView.Adapter<RecipesViewHolder> {
        private ArrayList<Recipe> recipes;
        private ArrayList<Recipe> copyList;
        private OnItemClickListener onItemClickListener;


        public RecipesAdapter(ArrayList<Recipe> recipes) {
            this.recipes = recipes;
            copyList = new ArrayList<>();
            copyList.addAll(recipes);
        }

        @Override
        public RecipesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_card_item, parent, false);

            return new RecipesViewHolder(parent.getContext(), itemView);
        }

        @Override
        public void onBindViewHolder(RecipesViewHolder holder, int position) {
            holder.bind(recipes.get(position), onItemClickListener);
        }

        @Override
        public int getItemCount() {
            return recipes.size();
        }

        public void filter(String queryText) {
            recipes.clear();

            if (queryText.isEmpty()) {
                recipes.addAll(copyList);
            } else {
                for (Recipe recipe : copyList) {
                    if (recipe.getTitle().toLowerCase().contains(queryText.toLowerCase())) {
                        recipes.add(recipe);
                    }
                }
            }

            notifyDataSetChanged();
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        public interface OnItemClickListener {
            void onItemClick(Recipe recipe, byte[] imgRecipe);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.searchBar);

        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }
}
