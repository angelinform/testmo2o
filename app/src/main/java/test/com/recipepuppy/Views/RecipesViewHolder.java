package test.com.recipepuppy.Views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;

import java.io.ByteArrayOutputStream;

import test.com.recipepuppy.Models.Recipe;
import test.com.recipepuppy.Models.Utils.PicassoClient;
import test.com.recipepuppy.R;

/**
 * Created by Angel on 24/3/18.
 */

public class RecipesViewHolder extends RecyclerView.ViewHolder {
    private Context context;
    private ImageView ivRecipe;
    private TextView tvNameRecipe;
    private TextView tvIngredientsRecipe;
    private TextView tvHrefRecipe;

    public RecipesViewHolder(Context context, View itemView) {
        super(itemView);
        this.context = context;
        ivRecipe = itemView.findViewById(R.id.iv_recipe);
        tvNameRecipe = itemView.findViewById(R.id.tv_name_recipe);
        tvIngredientsRecipe = itemView.findViewById(R.id.tv_ingredients_recipe);
        tvHrefRecipe = itemView.findViewById(R.id.tv_href_recipe);

    }

    public void bind(final Recipe recipe, final RecipeListActivity.RecipesAdapter.OnItemClickListener onItemClickListener){
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView ivRecipe = view.findViewById(R.id.iv_recipe);
                Drawable drawable = ivRecipe.getDrawable();
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                ByteArrayOutputStream  baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] bytesArray = baos.toByteArray();

                onItemClickListener.onItemClick(recipe, bytesArray);
            }
        });
        PicassoClient.downloadImage(context, recipe.getThumbnail(), ivRecipe);
        tvNameRecipe.setText(recipe.getTitle());
        tvIngredientsRecipe.setText(recipe.getIngredients());
        tvHrefRecipe.setText(recipe.getHref());
    }
}
