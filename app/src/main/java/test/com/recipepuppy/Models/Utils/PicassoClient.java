package test.com.recipepuppy.Models.Utils;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import test.com.recipepuppy.R;

/**
 * Created by Angel on 24/3/18.
 */

public class PicassoClient {
    public static void downloadImage(Context context, String url, ImageView imageView){
        if (url != null && url.length() > 0){
            Picasso.with(context).load(url).placeholder(R.drawable.placeholder_recipe_image).into(imageView);
        } else {
            Picasso.with(context).load(R.drawable.placeholder_recipe_image).into(imageView);
        }
    }
}
