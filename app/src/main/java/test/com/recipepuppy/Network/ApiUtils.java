package test.com.recipepuppy.Network;

/**
 * Created by Angel on 23/3/18.
 */

public class ApiUtils {
    public static final String BASE_URL = "http://www.recipepuppy.com/";

    public static RestClient getRestClient(){
        return RetrofitClient.getClient(BASE_URL).create(RestClient.class);
    }
}
