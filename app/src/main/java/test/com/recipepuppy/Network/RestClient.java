package test.com.recipepuppy.Network;

import retrofit2.Call;
import retrofit2.http.GET;
import test.com.recipepuppy.Models.RecipeResponse;

/**
 * Created by Angel on 23/3/18.
 */

public interface RestClient {
    @GET("api")
    Call<RecipeResponse> getRecipes();
}
